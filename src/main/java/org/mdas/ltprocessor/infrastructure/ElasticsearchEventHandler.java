package org.mdas.ltprocessor.infrastructure;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.jboss.logging.Logger;
import org.mdas.ltprocessor.infrastructure.config.ElasticsearchRestClient;
import org.mdas.ltprocessor.model.EventHandler;
import org.mdas.ltprocessor.model.Location;
import org.mdas.ltprocessor.model.Measure;

@ApplicationScoped
public class ElasticsearchEventHandler implements EventHandler<Measure> {

    private static final Logger logger = Logger.getLogger(ElasticsearchEventHandler.class);

    private final ElasticsearchRestClient client;
    private final boolean connected;

    public ElasticsearchEventHandler(ElasticsearchRestClient client) {
        this.client = client;
        this.connected = client.connect();
    }

    @Override
    public void handle(Measure measure) {
        logger.infov("Adding message {0} to index {1}", measure.toString(), client.indexName());

        final long timestamp = getTimestamp(measure.getTakenAt());
        final String id = measure.getMetadata().getId() + timestamp;

        Map<String, Object> documentMap = new HashMap<>();
        documentMap.put("temperature", measure.getTemperature());
        documentMap.put("sensorId", measure.getMetadata().getId());
        documentMap.put("area", measure.getMetadata().getArea());
        documentMap.put("location", parseLocation(measure));
        documentMap.put("timestamp", timestamp);

        client.send(id, documentMap);
    }

    private String parseLocation(Measure measure) {
        final Location location = measure.getMetadata().getLocation();
        if(location.getLatitude() < -90 || location.getLatitude() > 90){
            throw new IllegalArgumentException("Field latitude is out of valid range [-90.0,90.0]: " + location.getLatitude());
        } else if(location.getLongitude() < -180 || location.getLongitude() > 180 ){
            throw new IllegalArgumentException("Field longitude is out of valid range [-180.0,180.0]: " + location.getLongitude());
        }
        return location.getLatitude() + "," + location.getLongitude();
    }

    private long getTimestamp(String takenAt) {
        try {
            DateTimeFormatter rfc3339 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
            return LocalDateTime.parse(takenAt, rfc3339).toInstant(ZoneOffset.UTC).toEpochMilli();
        } catch (DateTimeException e) {
            throw new IllegalArgumentException("Field takenAt has an invalid format: " + takenAt);
        }
    }

    public void close() {
        client.close();
    }

    @Override
    public boolean isConnected() {
        return connected;
    }
}
