package org.mdas.ltprocessor.infrastructure;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.jboss.logging.Logger;
import org.mdas.ltprocessor.model.EventConsumer;
import org.mdas.ltprocessor.model.EventHandler;
import org.mdas.ltprocessor.model.Measure;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class KafkaEventConsumer implements EventConsumer {

   private static final Logger logger = Logger.getLogger(KafkaEventConsumer.class);

   private final EventHandler<Measure> handler;

   public KafkaEventConsumer(EventHandler<Measure> handler) {
      this.handler = handler;
   }

   @Incoming("measure-topic")
   @Override
   public void consume(Measure measure) {
      logger.infov("Consuming event from topic 'measure-topic'");
      logger.infov(measure.toString());
      handler.handle(measure);
   }
}