package org.mdas.ltprocessor.model;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class Location {
    private final Double latitude;
    private final Double longitude;

    @JsonbCreator
    public Location(@JsonbProperty("latitude") Double latitude,
                    @JsonbProperty("longitude") Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    @Override
    public String toString() {
        return "{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
