package org.mdas.ltprocessor.model;

public interface EventConsumer {
    void consume(Measure measure);
}
