package org.mdas.ltprocessor.model;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class Measure {
    private final Float temperature;
    private final String takenAt;
    private final Sensor sensor;

    @JsonbCreator
    public Measure(@JsonbProperty("temperature") Float temperature,
                   @JsonbProperty("takenAt") String takenAt,
                   @JsonbProperty("metadata") Sensor sensor) {
        this.temperature = temperature;
        this.takenAt = takenAt;
        this.sensor = sensor;
    }

    public Float getTemperature() {
        return temperature;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public Sensor getMetadata() {
        return sensor;
    }

    @Override
    public String toString() {
        return "{" +
                "temperature=" + temperature +
                ", takenAt='" + takenAt + '\'' +
                ", metadata=" + sensor +
                '}';
    }
}