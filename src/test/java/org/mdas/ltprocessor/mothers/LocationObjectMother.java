package org.mdas.ltprocessor.mothers;

import org.mdas.ltprocessor.model.Location;

public class LocationObjectMother {
    public static Location montcada() {
        return new Location(41.493662, 2.187147);
    }
}
