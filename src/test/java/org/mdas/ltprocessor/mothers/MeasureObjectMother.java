package org.mdas.ltprocessor.mothers;

import org.mdas.ltprocessor.model.Measure;

public class MeasureObjectMother {

    public static Measure measureFromMontcada() {
        return new Measure(29.5f, "2020-08-09T17:26:00Z", SensorObjectMother.montcada());
    }
}
