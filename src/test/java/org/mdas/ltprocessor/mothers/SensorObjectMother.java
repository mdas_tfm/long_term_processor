package org.mdas.ltprocessor.mothers;

import org.mdas.ltprocessor.model.Sensor;

public class SensorObjectMother {

    public static Sensor montcada() {
        return new Sensor("S1", "Montcada", LocationObjectMother.montcada());

    }
}