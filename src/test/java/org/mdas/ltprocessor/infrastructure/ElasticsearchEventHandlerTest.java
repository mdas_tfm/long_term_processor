package org.mdas.ltprocessor.infrastructure;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mdas.ltprocessor.infrastructure.config.ElasticsearchRestClient;
import org.mdas.ltprocessor.model.EventHandler;
import org.mdas.ltprocessor.model.Location;
import org.mdas.ltprocessor.model.Measure;
import org.mdas.ltprocessor.model.Sensor;
import org.mdas.ltprocessor.mothers.MeasureObjectMother;
import org.mdas.ltprocessor.mothers.SensorObjectMother;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ElasticsearchEventHandlerTest {

    @Mock
    ElasticsearchRestClient client;

    @Captor
    ArgumentCaptor<Map<String, Object>> documentCaptor;

    EventHandler<Measure> elasticsearchEventHandler;

    @BeforeEach
    public void setup() {
        elasticsearchEventHandler = new ElasticsearchEventHandler(client);
    }

    @Test
    public void shouldSendDocumentUsingClient() {
        // GIVEN
        Measure measure = MeasureObjectMother.measureFromMontcada();
        long timestamp = 1596993960000L;
        String id = measure.getMetadata().getId() + timestamp;
        String locationGeoHash = measure.getMetadata().getLocation().getLatitude() + ","
                + measure.getMetadata().getLocation().getLongitude();

        // WHEN
        elasticsearchEventHandler.handle(measure);

        // THEN
        verify(client).send(eq(id), documentCaptor.capture());

        Map<String, Object> document = documentCaptor.getValue();
        assertEquals(measure.getTemperature(), document.get("temperature"));
        assertEquals(measure.getMetadata().getArea(), document.get("area"));
        assertEquals(locationGeoHash, document.get("location"));
        assertEquals(timestamp, document.get("timestamp"));
    }

    @Test
    public void shouldThrowExceptionWhenDateIsInvalid() {
        // GIVEN
        Measure measure = new Measure(29.5f, "notADate", SensorObjectMother.montcada());

        // WHEN
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> elasticsearchEventHandler.handle(measure)
        );

        // THEN
        assertEquals("Field takenAt has an invalid format: notADate", exception.getMessage());
    }

    @Test
    public void shouldThrowExceptionWhenLatituteIsInvalid() {
        // GIVEN
        Measure measure = new Measure(29.5f, "2020-08-09T17:26:00Z",
                new Sensor("S1", "Montcada", new Location(99d, 2.5d)));

        // WHEN
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> elasticsearchEventHandler.handle(measure)
        );

        // THEN
        assertEquals("Field latitute is out of valid range [-90.0,90.0]: 99.0", exception.getMessage());
    }

    @Test
    public void shouldThrowExceptionWhenLongitudeIsInvalid() {
        // GIVEN
        Measure measure = new Measure(29.5f, "2020-08-09T17:26:00Z",
                new Sensor("S1", "Montcada", new Location(90d, -188d)));

        // WHEN
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> elasticsearchEventHandler.handle(measure)
        );

        // THEN
        assertEquals("Field longitude is out of valid range [-180.0,180.0]: -188.0", exception.getMessage());
    }

}