# Long term processor for kafka

[![pipeline status](https://gitlab.com/mdas_tfm/long_term_processor/badges/master/pipeline.svg)](https://gitlab.com/mdas_tfm/long_term_processor/-/commits/master)

This application reads data from a Kafka topic and stores it in an Elasticsearch index
for long term processing

## Requirements

* Java 11 or higher
* Elasticsearch server
* Kafka server

## Running the application in dev mode

### Dev mode configuration

* Using the *application.properties*, use the prefix *%dev.\<property>*, as in:

  `%dev.kafka.bootstrap.servers=192.168.99.100:32100`

**NOTE**: The default profile (no prefix) should be left only for kubernetes

* Environment variables take precedence over the *application.properties* file, for instance the kafka bootstrap servers could be set as follows:

  ```shell
  export KAFKA_BOOTSTRAP_SERVERS=172.17.0.2:321000
  ```

### Running the application

You can run your application in dev mode that enables live coding using:

```shell
./gradlew quarkusDev
```

## Packaging and running the application

### Versioning

This project uses the Gradle plugin `com.palantir.git-version` to version the artifacts according to git, tags plus current commit, on a clean repository it should only be the last tag.

### Build the JAR

The application can be packaged using `./gradlew quarkusBuild`.
It produces the `long_term_processor-1.0.0-SNAPSHOT-runner.jar` file in the `build` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `build/lib` directory.

The application is now runnable using `java -jar build/long_term_processor-1.0.0-SNAPSHOT-runner.jar`.

If you want to build an _über-jar_, just add the `--uber-jar` option to the command line:

```shell
./gradlew quarkusBuild --uber-jar
```

### Building and pushing a container image

This project uses Gitlab's container image registry by default, the image can be built and pushed as follows:

```shell
docker login -u username  -p deployment_token registry.gitlab.com
./gradlew quarkusBuild -Dquarkus.container-image.build=true -Dquarkus.container-image.push=true
```

### Deploy it in kubernetes

It requires kafka to be running on the *streams* namespace and ElasticSearch on the *elasticsearch*, as done by default in our automation.

After building and pushing the container image, execute the following commands:

```shell
$ kubectl create ns tfm
namespace/tfm
$ kubectl apply -f build/kubernetes/kubernetes.json -n tfm
serviceaccount/st-processor created
service/st-processor created
rolebinding.rbac.authorization.k8s.io/st-processor:view created
deployment.apps/st-processor created
ingress.extensions/st-processor created
$ kubectl get pods -n tfm
tfm           long-term-processor-795f5cf867-dgsqx             0/1     ContainerCreating   0          0s
tfm           long-term-processor-795f5cf867-dgsqx             0/1     Running             0          9s
tfm           long-term-processor-795f5cf867-dgsqx             1/1     Running             0          45s
```

## Send a new event to kafka topic

```json
      {
        "temperature": 22.209919609068038,
        "takenAt": "2020-05-31T14:59:02Z",
        "metadata": {
          "id": "asdfasdf834rasdf",
          "area": "MONTCADA",
          "location": {
            "latitude": 88,
            "longitude": 281.999
          }
        }
      }
```

```console
kaf produce measure-topic -b 172.17.0.3:32100 < msg.json
```
